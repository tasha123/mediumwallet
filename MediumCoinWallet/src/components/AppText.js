import React, { PropsTypes } from 'react';
import { Text } from 'react-native';
import * as globalStyles from '../styles/global';

const AppText = ({ children, style, ...rest }) => (
    <Text style={[globalStyles.COMMON_STYLES.text, style]} {...rest}>
    {children}
    </Text>
);

AppText.propTypes = {
    style: Text.propTypes.style,
    children: PropsTypes.node
};

export default AppText;
