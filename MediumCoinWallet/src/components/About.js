import React from 'react'
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native'
import { Actions } from 'react-native-router-flux'

const About = () => {
    const goToHome = () => {
        Actions.home()
    };
    return (
        <View style={styles.container}>
        <TouchableOpacity style={styles.touchBody} onPress={goToHome}>
            <Text>This is ABOUT</Text>
        </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    touchBody: {
        flex: 1,
        padding: 128,
        backgroundColor: '#fff470'
    }
});

export default About;