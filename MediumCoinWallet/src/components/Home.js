import React from 'react'
import {TouchableOpacity, Text, View, StyleSheet} from 'react-native';
import { Actions } from 'react-native-router-flux';

const Home = () => {
    const goToAbout = () => {
        Actions.about()
    };
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.touchBody} onPress={goToAbout}>
                <Text>This is HOME!</Text>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    touchBody: {
        flex: 1,
        padding: 128,
        backgroundColor: '#ffc8c3'
    }
});
export default Home