import React, { PropsTypes } from 'react';
import { StyleSheet, Text } from 'react-native';
import AppText from './AppText';
import * as globalStyles from '../styles/global';

const Title = ({ style }) => (
    <AppText style={[styles.title, style]}>
             title!!!!!
    </AppText>
);

Title.propTypes = {
    style: Text.propTypes.style,
    children: PropsTypes.node
};

const styles = StyleSheet.create({
    title: {
        fontFamily: 'HelveticaNeue-CondensedBold',
        fontSize: 18,
        color: globalStyles.HEADER_TEXT_COLOR,
        backgroundColor: `${globalStyles.BG_COLOR}99`
    }
});

export default Title;